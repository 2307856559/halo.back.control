package com.control.back.halo.basic.controller;

import java.util.Date;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import com.control.back.halo.basic.beans.DateEditor;

/**
 * Created by zhaohl on 2015/5/8.
 */
public class BaseController {

    protected static final String ERROR_PAGE = "/admin/common/error";

    @InitBinder
    protected void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        webDataBinder.registerCustomEditor(Date.class, new DateEditor(true));
    }
}
