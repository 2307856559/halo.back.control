package com.control.back.halo.manage.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.control.back.halo.basic.dao.IBaseDao;
import com.control.back.halo.basic.entity.User;
import com.control.back.halo.basic.log4j.Logger;
import com.control.back.halo.basic.service.impl.BaseServiceImpl;
import com.control.back.halo.basic.utils.UserUtils;
import com.control.back.halo.manage.dao.IAdminDao;
import com.control.back.halo.manage.entity.Admin;
import com.control.back.halo.manage.entity.Function;
import com.control.back.halo.manage.entity.Member;
import com.control.back.halo.manage.entity.Role;
import com.control.back.halo.manage.service.IAdminService;
import com.control.back.halo.manage.service.IFunctionService;
import com.control.back.halo.manage.service.IRoleService;

@Service
public class AdminServiceImpl extends BaseServiceImpl<Admin, Long> implements IAdminService {

    private static Logger    logger = Logger.getLogger(AdminServiceImpl.class);

    @Autowired
    private IAdminDao        adminDao;

    @Autowired
    private IRoleService     roleService;

    @Autowired
    private IFunctionService functionService;

    @Value("${halo.frame.variables.default-role}")
    private Long             defaultUserRoleId;

    @Override
    public IBaseDao<Admin, Long> getBaseDao() {
        return this.adminDao;
    }

    @Override
    public Admin findByUsername(String username) {
        return adminDao.findByUsername(username);
    }

    @Override
    public void saveOrUpdate(Admin admin) {
        if (admin.getId() != null) {
            Admin adminOne = find(admin.getId());
            User user = adminOne.getUser();
            if (admin.getUser() != null) {
                user.setUsername(admin.getUser().getUsername());
                if (StringUtils.isNoneBlank(admin.getUser().getPassword())) {
                    String password = new Sha256Hash(admin.getUser().getPassword()).toHex();
                    user.setPassword(password);
                }
            }
            Member member = adminOne.getMember();
            if (member == null) {
                adminOne.setMember(admin.getMember());
            } else {
                if (admin.getMember() != null) {
                    member.setBirthday(admin.getMember().getBirthday());
                    member.setSex(admin.getMember().getSex());
                    member.setName(admin.getMember().getName());
                }
            }
            update(adminOne);
        } else {
            if (admin.getUser() != null) {
                if (StringUtils.isNoneBlank(admin.getUser().getPassword())) {
                    String password = new Sha256Hash(admin.getUser().getPassword()).toHex();
                    admin.getUser().setPassword(password);
                }
            }
            Role role = roleService.find(defaultUserRoleId);
            admin.addToRole(role);
            save(admin);
        }
    }

    @Override
    public Boolean saveAdminRoles(Long id, Long[] roles) {
        try {
            Admin admin = find(id);
            Set<Role> rs = new HashSet<>(roles.length);
            for (Long roleId : roles) {
                Role role = roleService.find(roleId);
                rs.add(role);
            }
            admin.setRoles(rs);
            super.save(admin);
        } catch (Exception e) {
            logger.error("Exception params:[id:%s,roles:%s]", e, id.toString(), roles);
            return false;
        }
        return true;
    }

    @Override
    public List<Function> loadCurrentUserFunctions() {
        return functionService.loadFunctionByAdminId(UserUtils.getCurrentUser().getId());
    }

}
